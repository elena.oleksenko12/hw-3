const express = require('express');

const router = express.Router();
const { getUserProfile, deleteUser, editPasswordUser } = require('../controllers/userController');

router.get('/', getUserProfile);
router.delete('/', deleteUser);
router.patch('/password', editPasswordUser);

module.exports = {
  userRouter: router,
};

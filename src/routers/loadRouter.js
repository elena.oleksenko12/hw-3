const express = require('express');

const router = express.Router();
const {
  getLoads, createLoads, getLoadActive, updateLoadActive,
  getLoadById, updateMyLoadById, deleteLoadById, createLoadsById,
  getShippindInfo,
} = require('../controllers/loadController');

router.post('/', createLoads);
router.get('/', getLoads);
router.get('/active', getLoadActive);
router.patch('/active/state', updateLoadActive);
router.get('/:id', getLoadById);
router.put('/:id', updateMyLoadById);
router.delete('/:id', deleteLoadById);
router.post('/:id/post', createLoadsById);
router.get('/:id/shipping_info', getShippindInfo);

module.exports = {
  loadRouter: router,
};

const { Load, loadJoiSchema } = require('../models/Load');
const { Truck } = require('../models/Truck');
const { saveLoad } = require('../services/loadService');
const { getTruckForLoad } = require('../services/loadService');


const getLoads = async (req, res, next) => {
  if(req.user.role === 'DRIVER') {
    
    let loadsDriver = await Load.find({ assigned_to: req.user.userId, status: 'ASSIGNED'});

    return  res.status(200).json({
      'loads': await loadsDriver
    });

  } else {
    let loadsShipper = await Load.find({ created_by: req.user.userId }, '-__v');
      res.status(200).json({
        'loads': loadsShipper
      });
  }
}

async function createLoads(req, res, next) {
  try {
    const {
      name, payload, pickup_address, delivery_address, dimensions
    } = req.body;
    console.log( req.body);
    await loadJoiSchema.validateAsync({
      name, payload, pickup_address, delivery_address, dimensions,
    })

    const loadNew = await saveLoad({
      name, payload, pickup_address, delivery_address, dimensions,
    });
    loadNew.created_by = req.user.userId;

    await loadNew.save();

    return await res.status(200).json(
      {
        message: 'Load created successfully',
      }
    );
  } catch (error) {
    console.log(error.message);
    res.status(500).json({
      message: 'My Server error',
    });
  }
}

const getLoadActive = async (req, res, next) => {
  try {
    if (req.user.role === 'DRIVER') {
      console.log(req.user.userId);
      const activeLoad = await Load.findOne(
        { assigned_to: req.user.userId },
      );
     
          res.status(200).json({
            'load': activeLoad});
     
    } else {
      res.status(400).json({ message: 'Access is denied' });
    }
  } catch (err) {
    console.log(err.message);
    res.status(500).json({
      message: 'My Server error',
    });
  }
};

const updateLoadActive = async (req, res, next) => {
  try {
    if (req.user.role === 'DRIVER') {
      console.log(req.user.userId);
      let states = ['En route to Pick Up', 'En route to delivery', 'Arrived to delivery'];

      const activeLoad = await Load.findOne({ assigned_to: req.user.userId });
      if(await activeLoad.state===states[0]) {
        const activeLoadNew = await Load.findOneAndUpdate(
          { assigned_to: req.user.userId },
          { $set: { state: states[1] } },
        );
        await activeLoad.save();
        
          return res.status(200).json(
            {
              message: `Load state changed to ${states[1]}`,
            },
          );

      }
      if(await activeLoad.state===states[1]) {
        await Load.findOneAndUpdate(
          { assigned_to: req.user.userId },
          { $set: { state: states[2], status: 'SHIPPED' } }
        );
      }
      await activeLoad.save();

      const truck = await Truck.findOneAndUpdate({userId: req.user.userId, status: 'OL'},
      { $set: { status: 'IS' } },
      )
    
        return res.status(200).json(
          {
            message: `Load state changed to ${states[2]}`,
          },
        );
       
    } else {
      return res.status(400).json({ message: 'Access is denied' });
    }
  } catch (err) {
    console.log(err.message);
    res.status(500).json({
      message: 'My Server error ='+err.message,
    });
  }
};

async function createLoadsById(req, res, next) {
  const loadId = req.params.id;
  const load = await Load.findById(loadId);

  const truckForLoad = getTruckForLoad(
    load.dimensions.width,
    load.dimensions.length,
    load.dimensions.height,
    load.payload,
  );

  const activeTruck = await Truck.findOne(
    {
      status: 'IS',
      assigned_to: { $ne: null },
    },
  );
  console.log(`activeTruck.assigned_t=${await activeTruck.assigned_to}`);
  if (truckForLoad.length <= 1 && await activeTruck.type === truckForLoad[0]) {
    await dataUpdate(await activeTruck.assigned_to);
  } else if (truckForLoad.length <= 2 && (await activeTruck.type === truckForLoad[0]
     || await activeTruck.type === truckForLoad[1])) {
    await dataUpdate(await activeTruck.assigned_to);
  } else if (truckForLoad.length <= 3 && (await activeTruck.type === truckForLoad[0]
                || await activeTruck.type === truckForLoad[1]
                || await activeTruck.type === truckForLoad[2])) {
    
    await dataUpdate(await activeTruck.assigned_to);
  } else {
    await Load.findByIdAndUpdate({ _id: loadId }, {
      $set: {
        status: 'NEW',
        logs:
                    {
                      message: 'Driver was not found',
                      time: new Date(Date.now()),
                    },
      },
    });
  }

  async function dataUpdate(idDriver) {
    await activeTruck.updateOne({
      $set: { status: 'OL' },
    });
    await Load.findByIdAndUpdate({ _id: loadId }, {
      $set: {
        status: 'ASSIGNED',
        state: 'En route to Pick Up',
        assigned_to: idDriver,
        logs:
                    {
                      message: `Load assigned to driver with id ${idDriver}`,
                      time: new Date(Date.now()),
                    },
      },
    });
    return res.json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  }
}

const getLoadById = async (req, res, next) => {
  try {
    const load = await Load.findById(req.params.id)
     
        res.status(200).json(load);
    
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

const updateMyLoadById = async (req, res, next) => {
  try {
    const {
      name, payload, pickup_address, delivery_address, dimensions,
    } = req.body;
    const loadCurrent = await Load.findById({ _id: req.params.id, created_by: req.user.userId });
    if (await loadCurrent.status === 'NEW') {
       await Load.findByIdAndUpdate(
        { _id: req.params.id, created_by: req.user.userId },
        {
          $set: {
            name, payload, pickup_address, delivery_address, dimensions,
          },
        },
      )
      
          res.status(200).json({
            message: 'Load details changed successfully',
          });
      
    } else {
      res.status(400).json({
        message: 'Load details cannot be changed',
      });
    }
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

const deleteLoadById = async (req, res, next) => {
  try {
    const loadCurrent = await Load.findById({ _id: req.params.id, created_by: req.user.userId });
    if (await loadCurrent.status === 'NEW') {
      await Load.findByIdAndDelete(req.params.id)
      
          res.status(200).json({
            message: 'Load deleted successfully',
          });
       
    } else {
      res.status(400).json({
        message: 'Load cannot be deleted',
      });
    }
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

const getShippindInfo = async (req, res, next) => {
  try {
    console.log(req.params.id);
    const truckCurrent = await Truck.findOne({ status: 'OL' });

    const loadCurrent = await Load.findById({ _id: req.params.id });

    return await res.status(200).json({
      load: loadCurrent,
      truck: truckCurrent,
    });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

module.exports = {
  getLoads,
  createLoads,
  getLoadActive,
  updateLoadActive,
  getLoadById,
  createLoadsById,
  updateMyLoadById,
  deleteLoadById,
  getShippindInfo,

};

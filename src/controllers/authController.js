const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User, userJoiSchema } = require('../models/User');
const { saveUser } = require('../services/userService');

const registerUser = async (req, res, next) => {
  const { email, password, role } = req.body;

  if (!email || !password || !role) {
    res.status(400).json({ message: 'The field must not be empty' });
  }
  await userJoiSchema.validateAsync({ email, password, role });

  const user = await saveUser({ email, password, role });
  return await res.status(200).json({
    message: 'Profile created successfully',
  });
};

const loginUser = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });

  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = {
      email: user.email,
      role: user.role,
      userId: user._id,
      createdDate: user.createdDate,
    };
    const jwtToken = jwt.sign(payload, process.env.secret_jwt_key);
    return res.status(200).json({ jwt_token: jwtToken });
  }
  return res.status(403).json({ message: 'Not authorized' });
};

const userForgotPassword = async (req, res, next) => {
};

module.exports = {
  registerUser,
  loginUser,
  userForgotPassword,
};

const { Truck } = require('../models/Truck');

async function createTrucks(req, res, next) {
  try {
    const { type } = req.body;
    if (!type) {
      res.status(400).json({ message: 'The field must not be empty' });
    }
    const truck = new Truck({
      created_by: req.user.userId,
      // assigned_to: req.user.userId,
      type,
      status: 'IS',

    });
   await truck.save();
      res.status(200).json(
        {
          message: 'Truck created successfully',
        },
      );
  } catch (error) {
    res.status(500).json({
      message: 'Server error',
    });
  }
}

async function getTrucks(req, res, next) {
const trucks = await Truck.find({ created_by: req.user.userId }, '-__v');
      res.send({
        trucks,
      });
}

const getTruck = async (req, res, next) => {
  try {
    const truck = await Truck.findById(req.params.id);
    if (!truck) {
      res.status(400).json({ message: 'This truck not found' });
    }

    res.status(200).json({
      truck,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({
      message: 'My Server error',
    });
  }
};

const updateMyTruckById = async (req, res, next) => {
  const { type } = req.body;
   await Truck.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { type } },
  );
      res.status(400).json({ message: 'Truck details changed successfully' });
};

const deleteTruck = async (req, res, next) => {
  try {
  await Truck.findByIdAndDelete(req.params.id);
    res.status(200).json({
      message: 'Truck deleted successfully',
    });
  }catch(error) {
    res.status(500).json({ message: 'My Server error' });
  }
  }

const assignTruckById = async (req, res, next) => {
  try {
    const truckId = req.params.id;

    const truckAssign = await Truck.findByIdAndUpdate(
      { _id: truckId },
      { $set: { assigned_to: req.user.userId } },
    );
    await truckAssign.save()
        res.status(200).json({
          message: 'Truck assigned successfully',
        });
  } catch (err) {
    console.log(err.message);
    res.status(500).json({ message: 'My Server error' });
  }
};

module.exports = {
  createTrucks,
  getTrucks,
  getTruck,
  updateMyTruckById,
  deleteTruck,
  assignTruckById,
};
